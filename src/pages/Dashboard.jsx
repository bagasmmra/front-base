import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Card, Form, Image, Input, message, Modal, Typography, Upload} from "antd";
import {EditOutlined, UploadOutlined} from "@ant-design/icons";
import {selectAuth} from "@/redux/auth/selectors";
import styled from "styled-components";
import axios from "axios";
import {DashboardLayout} from "@/layout";
import {login} from "@/redux/auth/actions";

const {Meta} = Card;
const {Text, Title} = Typography;

const ResponsiveCard = styled(Card)`
  width: 80%;
  max-width: 500px;
  margin: 0 auto;
  text-align: center;

  @media (max-width: 768px) {
    width: 100%;
  }
`;

export default function Dashboard() {
    const {current} = useSelector(selectAuth);
    const dispatch = useDispatch();
    const [imageUrls, setImageUrls] = useState(null); // Declare a new state variable
    const [form] = Form.useForm();
    const [passwordField, setPasswordField] = useState('');
    const [phoneField, setPhoneField] = useState(current.phone);


    const [isModalVisible, setIsModalVisible] = useState(false);
    const handleInputPhone = (e) => {
        const newValue = e.target.value;
        setPhoneField(newValue);
    };

    const handleInputPassword = (e) => {
        const newValue = e.target.value;
        setPasswordField(newValue);
    };

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const axiosInstance = axios.create({
        baseURL: 'http://backend-test.prodishop.tech',
    });


    const handleFormSubmit = async () => {
        try {
            const formDataForUpload = {
                image: imageUrls, phone: phoneField, password: passwordField
            }

            // Send a POST request to update the user's profile
            const response = await axiosInstance.patch(`api/user/update-profile/${current.id}`, formDataForUpload);

            if (response) {
                const values = {
                    email: response.data.result.email, password: response.data.result.hashedPassword
                }
                dispatch(login(values))
            }


            setPasswordField("")
            setImageUrls(null)
            setPhoneField(current.phone)
            // Close the modal
            setIsModalVisible(false);
        } catch (error) {
            // Handle errors, possibly by displaying an error message
            console.error("Error updating profile:", error);
        }
    };


    const folderPath = `Argon/Profile/`;
    const uploadProps = {
        name: 'file', action: `https://api.cloudinary.com/v1_1/dowikaory/image/upload?folder=${folderPath}`, data: {
            upload_preset: 'icartsmr',
        }, onChange(info) {
            const {status, response} = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done' && response) {
                console.log(response.url)
                setImageUrls(response.url);
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    return (<DashboardLayout>
        <ResponsiveCard
            actions={[<Button
                icon={<EditOutlined/>}
                key="edit"
                onClick={showModal}
            >
                Edit Profile
            </Button>]}
        >
            <Image src={current.image} size={100}/>
            <Meta
                title={<Title level={4}>{current.name}</Title>}
                description={<>
                    <Text>Email: {current.email}</Text>
                    <br/>
                    <Text>Phone: {current.phone}</Text>
                    <br/>
                    <Text>Position: {current.position}</Text>
                    <br/>
                </>}
            />
        </ResponsiveCard>

        <Modal
            title="Edit Profile"
            visible={isModalVisible}
            onCancel={handleCancel}
            onOk={handleFormSubmit}
            okText="Save"
        >
            <Form form={form} layout="vertical" onFinish={handleFormSubmit}>
                <Form.Item label="Current Image">
                    {current.image ? (<Image src={current.image} width={100}/>) : (<span>No image available</span>)}
                </Form.Item>
                <Form.Item
                    label="Image"
                    name="image" // Add the "name" prop here
                >
                    <Upload {...uploadProps} listType="picture" multiple={false}>
                        <Button icon={<UploadOutlined/>}>
                            Click or drag images to upload
                        </Button>
                    </Upload>
                </Form.Item>
                <Form.Item label="Phone" name="phone"> {/* Add the "name" prop here */}
                    <Input onChange={handleInputPhone} value={phoneField}/>
                </Form.Item>
                <Form.Item label="New Password" name="password"> {/* Add the "name" prop here */}
                    <Input type="password" onChange={handleInputPassword} value={passwordField}/>
                </Form.Item>
            </Form>
        </Modal>
    </DashboardLayout>);
}