import React, {useEffect, useState} from 'react';
import {Button, DatePicker, message, Table} from 'antd';
import {DashboardLayout} from "@/layout";
import {useSelector} from "react-redux";
import {selectAuth} from "@/redux/auth/selectors";
import axios from "axios";
import moment from 'moment';

export default function Absen() {
    const {current} = useSelector(selectAuth);
    const [data, setData] = useState([])
    const [userData, setUserData] = useState([])
    // let dateKeys = []
    let today = moment().format('DD-M-YYYY');

    const axiosInstance = axios.create({
        baseURL: 'http://backend-test.prodishop.tech/',
    });

    const [selectedMonthYear, setSelectedMonthYear] = useState(new Date());

    const handleAbsentButtonClick = async (status) => {
        const data = {
            userId: current.id
        }
        if (status === "Entry") {
            try {
                console.log(current.id)
                const response = await axiosInstance.post(`api/absent-employee/absent-in`, data);

                if (response) {
                    console.log(response.data)
                    message.success(`${response.data.message}`);
                    setData([])
                    setUserData([])
                    window.location.reload();
                }

            } catch (error) {
                message.error(`${error.response.data.message}`);
                console.log("Error updating profile:", error);
            }
        } else {
            try {
                const response = await axiosInstance.post(`api/absent-employee/absent-out`, data);

                if (response) {
                    message.success(`${response.data.message}`);
                    console.log(response.data)
                    setData([])
                    setUserData([])
                    window.location.reload();
                }
            } catch (error) {
                message.error(`${error.response.data.message}`);
                console.log("Error updating profile:", error.response);
            }
        }
    };

    const handleDataTableLoad = async (date) => {
        setSelectedMonthYear(date);
        let selectedMonth
        let selectedYear
        if (date) {
            selectedMonth = date.format("M");
            selectedYear = date.format('YYYY');
        } else {
            selectedMonth = moment().format('M');
            selectedYear = moment().format('YYYY');
        }

        try {
            const response = await axiosInstance.get(`/api/absent-employee/list?month=${selectedMonth}&year=${selectedYear}`);

            if (current.role === 'Admin') {
                console.log('test', response.data);
                const myData = response.data.result;
                console.log(myData);
                setData(myData);
            } else {
                const filteredData = response.data.result.filter((item) => item.userId === current.id);
                console.log('test', filteredData[0]);
                setData(filteredData);
                setUserData(filteredData[0]);
            }
        } catch (error) {
            console.log(error);
            message.error(`${error.response.data.message}`);
        }
    };

    useEffect(() => {

        handleDataTableLoad();
    }, [setData, setUserData]);

    // // Filter the pivotTableArray by userId matching current.id
    // if (data){
    //     dateKeys = Object.keys(data[0] || {}).filter((key) => /^(\d{2}-\d{1,2}-\d{4})$/.test(key));
    // }
    //
    // console.log(dateKeys)


    const columns = [{
        title: 'Name', dataIndex: 'userName', key: 'userName',
    }, // {
        //     title: 'Status', dataIndex: 'status', key: 'status',
        // },
        // Modify the "Entry" column to display date and value
        {
            title: 'Entry', dataIndex: 'Entry', key: 'Entry', render: (Entry, record) => {
                const dateRows = [];
                Object.keys(Entry).forEach((date) => {
                    const value = Entry[date];
                    if (value) {
                        const formattedDate = `${date} : ${value}`;
                        dateRows.push(<div key={date}>{formattedDate}</div>);
                    } else {
                        const formattedDate = `${date} : `;
                        dateRows.push(<div key={date}>{formattedDate}</div>);
                    }
                });

                // Render the date-time rows
                return dateRows.length > 0 ? dateRows : 'No Entry Date';
            },
        },

        {
            title: 'Out', dataIndex: 'Out', key: 'Out', render: (Out, record) => {
                const dateRows = [];
                Object.keys(Out).forEach((date) => {
                    const value = Out[date];
                    if (value) {
                        const formattedDate = `${date} : ${value}`;
                        dateRows.push(<div key={date}>{formattedDate}</div>);
                    } else {
                        const formattedDate = `${date} : `;
                        dateRows.push(<div key={date}>{formattedDate}</div>);
                    }
                });

                // Render the date-time rows
                return dateRows.length > 0 ? dateRows : 'No Entry Date';
            },
        },];


    // Define a CSS class for the circular button
    const circleButtonStyle = {
        borderRadius: '50%',
        width: '100px',
        height: '100px',
        display: 'flex',
        margin: "auto",
        marginBottom: 20,
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: '16px',
    };

    const scrollConfig = {
        x: 800,
    };

    return (<DashboardLayout>
        <div>
            {current.role === 'User' && (
                <>
                    {userData?.Entry && userData.Entry[today] === null && (
                        <Button
                            style={circleButtonStyle}
                            type="default"
                            onClick={() => handleAbsentButtonClick('Entry')}
                        >
                            ENTRY
                        </Button>
                    )}
                    {userData?.Out && userData.Out[today] === null && (
                        <Button
                            style={circleButtonStyle}
                            type="primary"
                            onClick={() => handleAbsentButtonClick('Out')}
                        >
                            OUT
                        </Button>)}
                    {!userData && (<Button
                            style={circleButtonStyle}
                            type="default"
                            onClick={() => handleAbsentButtonClick("Entry")}
                        >
                            ENTRY
                        </Button>)}
                </>
            )}

        </div>
        <div>
            <DatePicker.MonthPicker
                selected={selectedMonthYear}
                onChange={handleDataTableLoad}
                showMonthYearPicker
                format="MM/YYYY" // Display format for the date picker
            />
        </div>
        <div>
            <Table
                dataSource={data}
                columns={columns}
                rowKey="id"
                scroll={scrollConfig} // Add the scroll prop here
            />
        </div>
    </DashboardLayout>);
}