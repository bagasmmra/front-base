import React from "react";

import CrudModule from "@/modules/CrudModule";
import EmployeeForm from "@/forms/EmployeeForm";

function Employee() {
    const entity = "user-employee";
    const searchConfig = {
        displayLabels: ["name"], searchFields: "name", outputValue: "_id",
    };

    const panelTitle = "Employee Panel";
    const dataTableTitle = "Employee Lists";
    const entityDisplayLabels = ["name"];

    const readColumns = [
        {
            title: "Name", dataIndex: "name",
        },
        {
            title: "Email", dataIndex: "email",
        },
        {
            title: "Phone", dataIndex: "phone",
        },
        {
            title: "Role", dataIndex: "role",
        },
        {
            title: "Position", dataIndex: "position",
        },
        // {
        //     title: "Password", dataIndex: "password",
        // },
    ];
    const dataTableColumns = [
        {
            title: "Name", dataIndex: "name",
        },
        {
            title: "Email", dataIndex: "email",
        },
        {
            title: "Phone", dataIndex: "phone",
        },
        {
            title: "Role", dataIndex: "role",
        },
        {
            title: "Position", dataIndex: "position",
        },
        {
            title: "Image", dataIndex: "image",
        },
        // {
        //     title: "Password", dataIndex: "password",
        // },
    ];

    const ADD_NEW_ENTITY = "Add Employee";
    const DATATABLE_TITLE = "Employee list";
    const ENTITY_NAME = "user-employee";
    const CREATE_ENTITY = "Create Employee";
    const UPDATE_ENTITY = "Update Employee";
    const config = {
        entity,
        panelTitle,
        dataTableTitle,
        ENTITY_NAME,
        CREATE_ENTITY,
        ADD_NEW_ENTITY,
        UPDATE_ENTITY,
        DATATABLE_TITLE,
        readColumns,
        dataTableColumns,
        searchConfig,
        entityDisplayLabels,
    };

    const formProps = {
        uploadFormItemProps: true,
    };

    return (<CrudModule
        createForm={<EmployeeForm/>}
        updateForm={<EmployeeForm keyState="update" isUpdateForm={true}/>}
        config={config}
    />);
}

export default Employee;