import React from "react";

import DefaultLayout from "../DefaultLayout";
import HeaderContent from "../HeaderContent";

import SidePanel from "@/components/SidePanel";
import {Layout} from "antd";

const {Content} = Layout;

export default function CrudLayout({children, config, sidePanelTopContent, sidePanelBottomContent, fixHeaderPanel,}) {
    return (
        <DefaultLayout>
            <Layout style={{minHeight: "100vh"}}>
                <HeaderContent/>
                <Layout className="site-layout">
                    <Content
                        className="site-layout-background"
                        style={{
                            padding: "50px 40px",
                            margin: "50px auto",
                            width: "100%",
                            overflow: "scroll", // Add this line to enable scrolling
                        }}
                    >
                        <SidePanel
                            config={config}
                            topContent={sidePanelTopContent}
                            bottomContent={sidePanelBottomContent}
                            fixHeaderPanel={fixHeaderPanel}
                        ></SidePanel>
                        {children}
                    </Content>
                </Layout>
            </Layout>
        </DefaultLayout>
    );
}