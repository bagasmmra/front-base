import React from "react";
import { Layout } from "antd";

const { Footer } = Layout;

const FooterContent = () => (
  <Footer style={{ textAlign: "center" }}>
    Ant Design ©2023
  </Footer>
);

export default FooterContent;