import React, {useState} from "react";

import {Link} from "react-router-dom";
import {Layout, Menu} from "antd";
import {DashboardOutlined, FileTextOutlined, SettingOutlined, TeamOutlined,} from "@ant-design/icons";
import {useSelector} from "react-redux";
import {selectAuth} from "@/redux/auth/selectors";

const {Sider} = Layout;
const {SubMenu} = Menu;

function Navigation() {
    const [collapsed, setCollapsed] = useState(false);
    const {current, isLoading, isSuccess} = useSelector(selectAuth);
    console.log('navigation', current)

    const onCollapse = () => {
        setCollapsed(!collapsed);
    };
    return (
        <>
            <Sider
                collapsible
                collapsed={collapsed}
                onCollapse={onCollapse}
                style={{
                    zIndex: 1000,
                }}
            >
                <div className="logo"/>
                <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
                    <Menu.Item key="1" icon={<DashboardOutlined/>}>
                        <Link to="/"/>
                            Profile
                    </Menu.Item>
                    {current.role === 'Admin' && (
                        <>
                            <Menu.Item key="2" icon={<DashboardOutlined/>}>
                                <Link to="/user-employee"/>
                                Employee
                            </Menu.Item>
                        </>
                    )}
                    <Menu.Item key="3" icon={<DashboardOutlined/>}>
                        <Link to="/absent-employee"/>
                        Absent
                    </Menu.Item>
                </Menu>
            </Sider>
        </>
    );
}

export default Navigation;