import React from "react";
import {useDispatch, useSelector} from "react-redux";

import {Avatar, Dropdown, Layout, Menu} from "antd";
import {logout} from "@/redux/auth/actions";
import uniqueId from "@/utils/uinqueId";
import {selectAuth} from "@/redux/auth/selectors";

const {Header} = Layout;

export default function HeaderContent() {
    const dispatch = useDispatch();
    const {current, isLoading, isSuccess} = useSelector(selectAuth);

    const menu = (
        <Menu>
            <Menu.Item key={`${uniqueId()}`} onClick={() => dispatch(logout())}>
                logout
            </Menu.Item>
        </Menu>
    );
    return (
        <Header
        className="site-layout-background"
        style={{padding: 0, background: "none"}}>
            <Dropdown overlay={menu} placement="bottomRight" arrow>
                <Avatar src={current.image} style={{width: 50, height: 50}}/>
            </Dropdown>
        </Header>
    );
}