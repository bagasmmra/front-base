import {ACCESS_TOKEN_NAME, API_BASE_URL} from "@/config/serverApiConfig";

import axios from "axios";
import errorHandler from "@/request/errorHandler";
import successHandler from "@/request/successHandler";
import storePersist from "@/redux/storePersist";

import {deleteCookie, getCookie, setCookie} from "./cookie";

export const token = {
    get: () => {
        return getCookie(ACCESS_TOKEN_NAME);
    }, set: (token) => {
        return setCookie(ACCESS_TOKEN_NAME, token);
    }, remove: () => {
        return deleteCookie(ACCESS_TOKEN_NAME);
    },
};

export const login = async (loginAdminData) => {
    try {
        const response = await axios.post(API_BASE_URL + `login`, loginAdminData);
        console.log(response)
        token.set(response.data.result.token);
        return successHandler(response);
    } catch (error) {
        return errorHandler(error);
    }
};

export const logout = () => {
    token.remove();
    storePersist.clear();
};