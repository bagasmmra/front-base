import React from "react";
import {Form, Input, Select} from "antd";
const {Option} = Select;

export default function EmployeeForm({isUpdateForm = false}) {
    return (<>
            <Form.Item
                label="E-mail"
                name="email"
                rules={[{
                    required: true, message: "Please input your Email!",
                },]}
            >
                <Input/>
            </Form.Item>
            {!isUpdateForm && (
                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{
                        required: true, message: "Please input your Password!"
                    },]}
                >
                    <Input type="password"/>
                </Form.Item>
            )}

            <Form.Item
                label="Name"
                name="name"
                rules={[{
                    required: true,
                },]}
            >
                <Input/>
            </Form.Item>
        <Form.Item
            label="Phone"
            name="phone"
            rules={[{
                required: true,
            },]}
        >
            <Input/>
        </Form.Item>

        <Form.Item
            label="Position"
            name="position"
            rules={[{
                required: true, message: "Please input your Position!"
            },]}
        >
            <Input/>
        </Form.Item>
        <Form.Item
            label="Role"
            name="role"
            rules={[
                {
                    required: true,
                    message: "Please select the Role!",
                },
            ]}
        >
            <Select>
                <Select.Option value={"User"}>User</Select.Option>
                <Select.Option value={"Admin"}>Admin</Select.Option>
            </Select>
        </Form.Item>
        </>);
}