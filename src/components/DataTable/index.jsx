import React, {useCallback, useEffect, useState} from "react";
import {Button, Dropdown, Image, message, Modal, PageHeader, Table} from "antd";
import {EllipsisOutlined, ExclamationCircleOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import {crud} from "@/redux/crud/actions";
import {selectListItems} from "@/redux/crud/selectors";
import uniqueId from "@/utils/uinqueId";
import axios from 'axios';
import { v4 as uuid } from 'uuid';

const {confirm} = Modal;


export default function DataTable({config, DropDownRowMenu, AddNewItem}) {
    let {entity, dataTableColumns, dataTableTitle} = config;

    dataTableColumns = dataTableColumns.map((column) => {
        if (column.dataIndex === "image") {
            return {
                ...column, render: (image) => <Image src={image} width={100}/>,
            };
        }
        return column;
    });

    dataTableColumns.push({
        title: "", render: (row) => (<Dropdown overlay={DropDownRowMenu({row})} trigger={["click"]}>
            <EllipsisOutlined style={{cursor: "pointer", fontSize: "24px"}}/>
        </Dropdown>),
    });

    const handelDataTableLoad = useCallback((pagination) => {
        dispatch(crud.list(entity, pagination.current));
    }, []);

    useEffect(() => {
        // console.log('disini masuk')
        dispatch(crud.list(entity));
    }, []);

    const {result: listResult, isLoading: listIsLoading} = useSelector(selectListItems);

    const {pagination, items} = listResult;

    const dispatch = useDispatch();

    return (<>
        <PageHeader
            onBack={() => window.history.back()}
            title={dataTableTitle}
            ghost={false}
            extra={[<Button onClick={handelDataTableLoad} key={`${uniqueId()}`}>
                Refresh
            </Button>, <AddNewItem key={`${uniqueId()}`} config={config}/>,]}
            style={{
                padding: "20px 0px",
            }}
        ></PageHeader>
        <Table
            columns={dataTableColumns}
            rowKey={(item) => item.id}
            dataSource={items}
            pagination={pagination}
            loading={listIsLoading}
            onChange={handelDataTableLoad}
        />
    </>);
}