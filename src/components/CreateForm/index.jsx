import React, {useEffect, useState} from "react";

import {useDispatch, useSelector} from "react-redux";
import {crud} from "@/redux/crud/actions";
import {useCrudContext} from "@/context/crud";
import {selectCreatedItem} from "@/redux/crud/selectors";

import {Button, Form, message, Upload} from "antd";
import Loading from "@/components/Loading";
import {UploadOutlined} from "@ant-design/icons";

export default function CreateForm({config, formElements, uploadFormItemProps}) {
    let {entity} = config;
    console.log(formElements)
    const dispatch = useDispatch();
    const {isLoading, isSuccess} = useSelector(selectCreatedItem);
    const {crudContextAction} = useCrudContext();
    const {panel, collapsedBox, readBox} = crudContextAction;
    const [form] = Form.useForm();
    const [imageUrls, setImageUrls] = useState(null); // Declare a new state variable

    const onSubmit = (fieldsValue) => {
        console.log(fieldsValue)
        // Create a new object to store the parsed form values
        const formData = {};

        // Iterate through the fieldsValue object and parse specific fields to integers
        for (const fieldName in fieldsValue) {
            if (fieldName === 'maxClaims' || fieldName === 'discount' || fieldName === 'maxDiscount' || fieldName === 'maxUserUsed' || fieldName === 'transMinimum') {
                formData[fieldName] = parseInt(fieldsValue[fieldName]);
            } else {
                formData[fieldName] = fieldsValue[fieldName];
            }
        }
        console.log(entity, formData)

        dispatch(crud.create(entity, formData));
    };


    const folderPath = `Kilapin/${entity}/`;
    const uploadProps = {
        name: 'file', action: `https://api.cloudinary.com/v1_1/dtyji62ve/image/upload?folder=${folderPath}`, data: {
            upload_preset: 'yjjew3l8',
        }, onChange(info) {
            const {status, response} = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done' && response) {
                console.log(response.url)
                setImageUrls(response.url);
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };


    useEffect(() => {
        form.setFieldsValue({
            image: imageUrls
        });
        if (isSuccess) {
            readBox.open();
            collapsedBox.open();
            panel.open();
            form.resetFields();
            dispatch(crud.resetAction("create"));
            dispatch(crud.list(entity));
        }
    }, [isSuccess, imageUrls]);

    return (
        <Loading isLoading={isLoading}>
        <Form form={form} layout="vertical" onFinish={onSubmit}>
            {formElements}
            {formElements.props.uploadFormItemProps && ( // cek apakah ini form create
                <Form.Item
                    label="Image"
                    name="image"
                    rules={[{
                        required: true, message: "Please upload an image!",
                    }]}
                >
                    <Upload {...uploadProps} listType="picture" multiple={true}>
                        <Button icon={<UploadOutlined/>}>
                            Click or drag images to upload
                        </Button>
                    </Upload>
                </Form.Item>)}
            <Form.Item>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    </Loading>);
}