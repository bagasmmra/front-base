import React, {useEffect, useState} from "react";
import dayjs from "dayjs";
import {useDispatch, useSelector} from "react-redux";
import {crud} from "@/redux/crud/actions";
import {useCrudContext} from "@/context/crud";
import {selectUpdatedItem} from "@/redux/crud/selectors";

import {Button, Form, message, Upload} from "antd";
import Loading from "@/components/Loading";
import {UploadOutlined} from "@ant-design/icons";

export default function UpdateForm({config, formElements}) {
    let {entity} = config;
    const dispatch = useDispatch();
    const {current, isLoading, isSuccess} = useSelector(selectUpdatedItem);
    const {state, crudContextAction} = useCrudContext();
    const {panel, collapsedBox, readBox} = crudContextAction;
    const [form] = Form.useForm();
    const [imageUrls, setImageUrls] = useState(null); // Declare a new state variable


    const onSubmit = (fieldsValue) => {
        const id = current.id;
        dispatch(crud.update(entity, id, fieldsValue));
    };

    useEffect(() => {
        if (current) {
            if (current.birthday) {
                current.birthday = dayjs(current.birthday);
            }
            if (current.date) {
                current.date = dayjs(current.date);
            }
            form.setFieldsValue(current);
        }
    }, [current]);

    useEffect(() => {
        form.setFieldsValue({
            image: imageUrls
        });
        if (isSuccess) {
            readBox.open();
            collapsedBox.open();
            panel.open();
            form.resetFields();
            dispatch(crud.resetAction("update"));
            dispatch(crud.list(entity));
        }
    }, [isSuccess, imageUrls]);

    const {isEditBoxOpen} = state;

    const folderPath = `Kilapin/${entity}/`;
    const uploadProps = {
        name: 'file',
        action: `https://api.cloudinary.com/v1_1/dtyji62ve/image/upload?folder=${folderPath}`,
        data: {
            upload_preset: 'yjjew3l8',
        },
        onChange(info) {
            const {status, response} = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done' && response) {
                setImageUrls(response.url);
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    const show = isEditBoxOpen
        ? {display: "block", opacity: 1}
        : {display: "none", opacity: 0};
    return (
        <div style={show}>
            <Loading isLoading={isLoading}>
                <Form form={form} layout="vertical" onFinish={onSubmit}>
                    {formElements}
                    {formElements.props.uploadFormItemProps && ( // cek apakah ini form create
                        <Form.Item
                            label="Image"
                            name="image"
                            rules={[
                                {
                                    required: true,
                                    message: "Please upload an image!",
                                },
                            ]}
                        >
                            <Upload {...uploadProps} listType="picture" multiple={true}>
                                <Button icon={<UploadOutlined/>}>
                                    Click or drag images to upload
                                </Button>
                            </Upload>
                        </Form.Item>
                    )}
                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Loading>
        </div>
    );
}